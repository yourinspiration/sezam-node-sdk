var https        = require('https');
var crypto       = require('crypto')
var bcryptNodejs = require('bcrypt-nodejs')

function Sezam(options) {
  if (options !== undefined) {
    this.options = options;
  } else {
    this.options = {};
  }
}

Sezam.prototype.create = function(user, cb) {
  if (user.username === undefined)
    throw new Error('you must provide a username');
  if (user.email === undefined)
    throw new Error('you must provide an email');
  if (user.password === undefined)
    throw new Error('you must provide a password');
  if (user.roles === undefined)
    throw new Error('you must provide roles');
  if (user.enabled === undefined)
    throw new Error('you must provide enabled');

  var account = {
    username: user.username,
    email: user.email,
    password: user.password,
    roles: user.roles,
    enabled: user.enabled
  };

  var data = JSON.stringify(account);

  var httpsOptions = {
    host: 'sezam.yourinspiration.de',
    path: '/rest/accounts',
    method: 'POST',
    headers: {
      'Authorization': 'Basic ' + new Buffer(this.options.id + ':' +
          this.options.key).toString('base64'),
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(data)
    }
  };

  var request = https.request(httpsOptions, function(response) {
    var str = '';

    response.on('data', function(chunk) {
      str += chunk;
    });

    response.on('end', function() {
      if (response.statusCode === 401) {
        return cb(new Error('Could not authorize your request. ' +
          'Check your sezam application id and key!'));
      }
      if (response.statusCode === 200 || response.statusCode === 201) {
        account = JSON.parse(str);
        return cb(null, account);
      }
      if (response.statusCode === 409) {
        var err = new Error('This username or email is already allocated');
        return cb(err, null);
      }
      return cb(new Error('An unexpected error has occured.'), null);
    });
  });
  request.write(data);
  request.end();
}

Sezam.prototype.update = function(user, cb) {
  if (user.id === undefined)
    throw new Error('user must have an id to be updated');
  if (user.username === undefined)
    throw new Error('you must provide a username');
  if (user.email === undefined)
    throw new Error('you must provide an email');
  if (user.password === undefined)
    throw new Error('you must provide a password');
  if (user.roles === undefined)
    throw new Error('you must provide roles');
  if (user.enabled === undefined)
    throw new Error('you must provide enabled');

  var account = {
    id: user.id,
    username: user.username,
    email: user.email,
    password: user.password,
    roles: user.roles,
    enabled: user.enabled
  };

  var data = JSON.stringify(account);

  var httpsOptions = {
    host: 'sezam.yourinspiration.de',
    path: '/rest/accounts',
    method: 'PUT',
    headers: {
      'Authorization': 'Basic ' + new Buffer(this.options.id + ':' +
          this.options.key).toString('base64'),
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(data)
    }
  };

  var request = https.request(httpsOptions, function(response) {
    var str = '';

    response.on('data', function(chunk) {
      str += chunk;
    });

    response.on('end', function() {
      if (response.statusCode === 401) {
        return cb(new Error('Could not authorize your request. ' +
          'Check your sezam application id and key!'));
      }
      if (response.statusCode === 200 || response.statusCode === 201) {
        return cb(null, account);
      }
      if (response.statusCode === 404) {
        return cb(new Error('User not found.'));
      }
      if (response.statusCode === 409) {
        return cb(new Error('This username or email is already allocated'));
      }
      return cb(new Error('An unexpected error has occured.'));
    });
  });
  request.write(data);
  request.end();
}

Sezam.prototype.remove = function(user, cb) {

  var httpsOptions = {
    host: 'sezam.yourinspiration.de',
    path: '/rest/accounts/' + user.id,
    method: 'DELETE',
    headers: {
      'Authorization': 'Basic ' + new Buffer(this.options.id + ':' +
          this.options.key).toString('base64')
    }
  };

  https.request(httpsOptions, function(response) {
    var str = '';

    response.on('data', function(chunk) {
      str += chunk;
    });

    response.on('end', function() {
      if (response.statusCode === 401) {
        return cb(new Error('Could not authorize your request. ' +
          'Check your sezam application id and key!'));
      }
      if (response.statusCode === 200) {
        return cb(null);
      }
      if (response.statusCode === 404) {
        return cb(new Error('User not found.'));
      }
      return cb(new Error('An unexpected error has occured.'));
    });

  }).end();

}

Sezam.prototype.removeAll = function(cb) {

  var httpsOptions = {
    host: 'sezam.yourinspiration.de',
    path: '/rest/accounts',
    method: 'DELETE',
    headers: {
      'Authorization': 'Basic ' + new Buffer(this.options.id + ':' +
          this.options.key).toString('base64')
    }
  };

  https.request(httpsOptions, function(response) {
    var str = '';

    response.on('data', function(chunk) {
      str += chunk;
    });

    response.on('end', function() {
      if (response.statusCode === 401) {
        return cb(new Error('Could not authorize your request. ' +
          'Check your sezam application id and key!'));
      }
      if (response.statusCode === 200) {
        return cb(null);
      }
      return cb(new Error('An unexpected error has occured.'));
    });

  }).end();

}
Sezam.prototype.findById = function(id, cb) {

  var httpsOptions = {
    host: 'sezam.yourinspiration.de',
    path: '/rest/accounts/' + id,
    method: 'GET',
    headers: {
      'Authorization': 'Basic ' + new Buffer(this.options.id + ':' +
          this.options.key).toString('base64')
    }
  };

  https.request(httpsOptions, function(response) {
    var str = '';

    response.on('data', function(chunk) {
      str += chunk;
    });

    response.on('end', function() {
      if (response.statusCode === 401) {
        return cb(new Error('Could not authorize your request. ' +
          'Check your sezam application id and key!'));
      }
      if (response.statusCode === 200) {
        var account = JSON.parse(str);
        return cb(null, account);
      }
      if (response.statusCode === 404) {
        return cb(null, null);
      }
      return cb(new Error('An unexpected error has occured.'));
    });

  }).end();

}

Sezam.prototype.findAll = function(cb) {

  var httpsOptions = {
    host: 'sezam.yourinspiration.de',
    path: '/rest/accounts',
    method: 'GET',
    headers: {
      'Authorization': 'Basic ' + new Buffer(this.options.id + ':' +
          this.options.key).toString('base64')
    }
  };

  https.request(httpsOptions, function(response) {
    var str = '';

    response.on('data', function(chunk) {
      str += chunk;
    });

    response.on('end', function() {
      if (response.statusCode === 401) {
        return cb(new Error('Could not authorize your request. ' +
          'Check your sezam application id and key!'));
      }
      if (response.statusCode === 200) {
        var accounts = JSON.parse(str);
        return cb(null, accounts);
      }
      return cb(new Error('An unexpected error has occured.'));
    });

  }).end();

}

Sezam.prototype.findByUsername = function(username, cb) {

  var httpsOptions = {
    host: 'sezam.yourinspiration.de',
    path: '/rest/accounts/username/' + username,
    method: 'GET',
    headers: {
      'Authorization': 'Basic ' + new Buffer(this.options.id + ':' +
          this.options.key).toString('base64')
    }
  };

  https.request(httpsOptions, function(response) {
    var str = '';

    response.on('data', function(chunk) {
      str += chunk;
    });

    response.on('end', function() {
      if (response.statusCode === 401) {
        return cb(new Error('Could not authorize your request. ' +
          'Check your sezam application id and key!'));
      }
      if (response.statusCode === 200) {
        var account = JSON.parse(str);
        return cb(null, account);
      }
      if (response.statusCode === 404) {
        return cb(null, null);
      }
      return cb(new Error('An unexpected error has occured.'));
    });

  }).end();

}

Sezam.prototype.findByEmail = function(email, cb) {

  var httpsOptions = {
    host: 'sezam.yourinspiration.de',
    path: '/rest/accounts/email/' + email,
    method: 'GET',
    headers: {
      'Authorization': 'Basic ' + new Buffer(this.options.id + ':' +
          this.options.key).toString('base64')
    }
  };

  https.request(httpsOptions, function(response) {
    var str = '';

    response.on('data', function(chunk) {
      str += chunk;
    });

    response.on('end', function() {
      if (response.statusCode === 401) {
        return cb(new Error('Could not authorize your request. ' +
          'Check your sezam application id and key!'));
      }
      if (response.statusCode === 200) {
        var account = JSON.parse(str);
        return cb(null, account);
      }
      if (response.statusCode === 404) {
        return cb(null, null);
      }
      return cb(new Error('An unexpected error has occured.'));
    });

  }).end();

}

Sezam.prototype.authenticate = function(username, password, cb) {

  var httpOptions = {
    host: 'sezam.yourinspiration.de',
    path: '/rest/accounts/username/' + username,
    method: 'GET',
    headers: {
      'Authorization': 'Basic ' + new Buffer(this.options.id + ':' +
          this.options.key).toString('base64')
    }
  };

  https.request(httpOptions, function(response) {
    var str = '';

    response.on('data', function(chunk) {
      str += chunk;
    });

    response.on('end', function() {
      if (response.statusCode === 401) {
        if (cb) {
          return cb(new Error('Could not authorize your request. ' +
            'Check your sezam application id and key!'));
        } else {
          throw new Error('Could not authorize your request. ' +
            'Check your sezam application id and key!');
        }
      }
      if (response.statusCode === 200) {
        var account = JSON.parse(str);
        if (compare(this.options.digest, password, account.password)) {
          return cb(null, account);
        } else {
          return cb(null, null);
        }
      }
      if (response.statusCode === 404) {
        return cb(null, null);
      }
      return cb(new Error('An unexpected error has occured.'));
    });

  }).end();

}

Sezam.prototype.basicAuth = function(options) {
  return function(req, res, next) {
    var auth = req.headers.authorization;
    if (!auth) {
      res.setHeader('WWW-Authenticate', 'Basic realm="sezam"');
      return res.status(401).end();
    }

    var parts = auth.split(' ');
    if ('basic' != parts[0].toLowerCase()) {
      res.setHeader('WWW-Authenticate', 'Basic realm="sezam"');
      return res.status(401).end();
    }

    if (!parts[1]) {
      res.setHeader('WWW-Authenticate', 'Basic realm="sezam"');
      return res.status(401).end();
    }

    auth = parts[1];

    auth = new Buffer(auth, 'base64').toString();
    auth = auth.match(/^([^:]+):(.+)$/);
  
    if (!auth) {
      res.setHeader('WWW-Authenticate', 'Basic realm="sezam"');
      return res.status(401).end();
    }

    var httpOptions = {
      host: 'sezam.yourinspiration.de',
      path: '/rest/accounts/username/' + auth[1],
      method: 'GET',
      headers: {
        'Authorization': 'Basic ' + new Buffer(options.id + ':' +
            options.key).toString('base64')
      }
    };


    https.request(httpOptions, function(response) {
      var str = '';

      response.on('data', function(chunk) {
        str += chunk;
      });

      response.on('end', function() {
        if (response.statusCode === 401) {
          return next(new Error('Could not authorize your request. ' +
              'Check your sezam application id and key!'));
        }
        if (response.statusCode === 200) {
          var account = JSON.parse(str);
          if (compare(options.digest, auth[2], account.password)) {
            req.user = account;
            return next();
          } else {
            res.setHeader('WWW-Authenticate', 'Basic realm="sezam"');
            return res.status(401).end();
          }
        }
        return res.status(401).end();
      });

    }).end();

  }
}

md5 = function(password) {
  return crypto.createHash('md5').update(password).digest('hex');
}

sha1 = function(password) {
  return crypto.createHash('sha1').update(password).digest('hex');
}

sha256 = function(password) {
  return crypto.createHash('sha256').update(password).digest('hex');
}

sha512 = function(password) {
  return crypto.createHash('sha512').update(password).digest('hex');
}

compare = function(digest, password, hash) {
  if (digest === 'MD5') {
    return md5(password) == hash;
  } else if (digest === 'SHA-1') {
    return sha1(password) == hash;
  } else if (digest === 'SHA-256') {
    return sha256(password) == hash;
  } else if (digest === 'SHA-512') {
    return sha512(password) == hash;
  } else {
    return bcryptNodejs.compareSync(password, hash);
  }
}

module.exports = Sezam;
