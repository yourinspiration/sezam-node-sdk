# About
This node.js module is a simple to use client SDK for the
[Sezam Authentication Service](https://sezam.yourinspiration.de/).
It simply wraps the [Sezam REST API](https://sezam.yourinspiration.de/#api).

# Installation

```
npm install sezam --save
```

# Usage

```javascript
var Sezam = require('sezam');

var options = {
  id: 'YOUR-APPLICATION-ID',
  key: 'YOUR-APPLICATION-KEY',
  digest: 'YOUR-APPLICATION-DIGEST'
};

var sezam = new Sezam(options);
```

# Basic-Auth for express

```javascript
var express = require('express');
var Sezam = require('sezam');

var options = {
  id: 'YOUR-APPLICATION-ID',
  key: 'YOUR-APPLICATION-KEY',
  digest: 'YOUR-APPLICATION-DIGEST'
};

var sezam = new Sezam(options);
var app = express();

app.use(sezam.basicAuht(options));

app.get('/', function(req, res, next) {
  var currentUser = req.user;
});
```
# API

## Find all users

```javascript
sezam.findAll(function(err, users) {

});
```

## Find user by id

```javascript
sezam.findById(id, function(err, user) {

});
```

## Find user by username

```javascript
sezam.findByUsername(username, function(err, user) {

});
```

## Find user by email

```javascript
sezam.findByEmail(email, function(err, user) {

});
```

## Create user

```javascript
var user = {
  username: 'max',
  email: 'max@mustermann.de',
  password: '1234',
  roles: 'USER',
  enabled: true
});

sezam.create(user, function(err, createdUser) {

});
```

## Update user

```javascript
sezam.update(someUser, function(err, updatedUser) {

}):
```

## Remove user

```javascript
sezam.remove(someUser.id, function(err) {

});
```

## Remove all users

```javascript
sezam.removeAll(function(err) {

});
```

## Autenticate by username and password

```javascript
sezam.authenticate(username, password, function(err, user) {
  // user is null if username and password did not match.
});
```

